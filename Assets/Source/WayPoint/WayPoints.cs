using UnityEngine;

public class WayPoints : MonoBehaviour
{
    [SerializeField] private Transform[] _allWayPoint;
    public Transform[] AllWayPoint => _allWayPoint;
    public Transform GetRandomPoint()
    {
        int index = Random.Range(0, _allWayPoint.Length);
        return _allWayPoint[index];
    }
    private void Awake() => _allWayPoint = GetComponentsInChildren<Transform>();

}
