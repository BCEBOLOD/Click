using UnityEngine;

public class CountdownStart : MonoBehaviour
{

    public UnityEngine.Events.UnityAction e_sendStartGameplay;

    [SerializeField] private GameRules _gameRules;
    [SerializeField] private TMPro.TextMeshProUGUI _countdownText;
    [SerializeField] private float _startCounterTimer;
    [SerializeField] private float _counterStarting;// начальное время для старта отсчета  
    [SerializeField] private Animator _animator;
    private void Start()
    {
        OnTransparency();
        _gameRules.e_stopAction += () => { OnTransparency(); };
        _gameRules.FirstTouch.e_startWave += OnStartCoroutine;
        _gameRules.e_restartGame += () => { OnStartCoroutine(); };

    }
    private void OnStartCoroutine()
    {
        _counterStarting = _startCounterTimer;
        if (!_countdownText.gameObject.activeSelf)
            _countdownText.gameObject.SetActive(true);
        StartCoroutine(StartCountdown());
        OffTransparency();
        _countdownText.text = _counterStarting.ToString();
    }
    private System.Collections.IEnumerator StartCountdown()
    {
        while (_counterStarting > 0)
        {

            yield return new WaitForSeconds(1f);
            _counterStarting--;
            _countdownText.text = _counterStarting.ToString();
            if (_counterStarting <= 0)
            {
                StopCoroutine(StartCountdown());
                _countdownText.text = _counterStarting.ToString();
                yield return new WaitForSeconds(1f);
                _countdownText.text = "Начало игры";
                yield return new WaitForSeconds(1.5f);
                _countdownText.gameObject.SetActive(false);
                e_sendStartGameplay?.Invoke();
                print("Count");
            }
        }
    }

    private void OffTransparency()
    {
        _animator.SetTrigger(AnimPref.OnTRANSPARENCY);
    }
    private void OnTransparency()
    {
        _animator.SetTrigger(AnimPref.DefaultTRANSPARENCY);
    }
}
