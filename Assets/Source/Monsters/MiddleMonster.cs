using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiddleMonster : BaseMonster
{
    public override void AddComplexity()
    {
        _maxHP += Random.Range(2, 3);
        _currentMoveSpeed += Random.Range(0.002f, 0.003f);
        _rewardPoint += 6;
    }
}
