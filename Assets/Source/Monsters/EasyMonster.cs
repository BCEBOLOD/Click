using UnityEngine;
public class EasyMonster : BaseMonster
{
    public override void AddComplexity()
    {
        _maxHP += Random.Range(1, 3);
        _currentMoveSpeed += Random.Range(0.001f, 0.003f);
        _rewardPoint += 3;
    }
}
