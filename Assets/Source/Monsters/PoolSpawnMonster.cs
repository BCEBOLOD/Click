using UnityEngine;

public class PoolSpawnMonster : MonoBehaviour
{
    public UnityEngine.Events.UnityAction<BaseMonster> e_sendMonster;
    [SerializeField] private GameObject _easyMonster;
    [SerializeField] private GameObject _middleMonster;
    [SerializeField] private GameObject _hardMonster;
    //
    [SerializeField] private int _countEasy, _countMiddle, _countHard;
    public int CountEasy => _countEasy;
    public int CountMiddle => _countMiddle;
    public int CountHard => _countHard;
    //
    private async void Start()
    {
        await System.Threading.Tasks.Task.Delay(System.TimeSpan.FromSeconds(0.5f));
        InitPool(_easyMonster, _countEasy);
        InitPool(_middleMonster, _countMiddle);
        InitPool(_hardMonster, _countHard);
    }

    private void InitPool(GameObject prefab, int count)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject pt = Instantiate(prefab, transform.position, prefab.transform.rotation);
            pt.transform.SetParent(transform);
            e_sendMonster?.Invoke(pt.GetComponent<BaseMonster>());
            pt.SetActive(false);
        }
    }
}
