
using UnityEngine;

public class HardMonster : BaseMonster
{
    public override void AddComplexity()
    {
        _maxHP += Random.Range(1, 5);
        _currentMoveSpeed += Random.Range(0.001f, 0.002f);
        _rewardPoint += 8;
    }
}
