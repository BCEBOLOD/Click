using UnityEngine;

public class SpawningMonster : MonoBehaviour
{
    public UnityEngine.Events.UnityAction e_sendIncrementSpawn;
    public UnityEngine.Events.UnityAction<int> e_sendDecrementSpawn;
    public UnityEngine.Events.UnityAction e_addComplexity;
    public UnityEngine.Events.UnityAction e_playClipSpawnMonster;
    public GameMode GameMode => _gameMode;
    public int CountSpawning => _countSpawning;
    public GameRules GameRules => _gameRules;
    [SerializeField] private PoolSpawnMonster _poolSpawnMonster;
    [SerializeField] private TimeSpawnMonster _timeSpawn;
    [SerializeField] private int _percentEasy, _percentMiddle;
    [SerializeField] private float _currentTimer;
    [SerializeField] private System.Collections.Generic.List<BaseMonster> _allMonsters;

    [SerializeField] private int _countSpawning;// Сколько уже было заспавнено
    [SerializeField] private int _numberForAddComplecity;//На каком спавне увеличить сложность?\    
    [SerializeField] private int[] _countTypeMonster;
    [SerializeField] private bool[] _isMonsterActive;
    private GameMode _gameMode;
    private GameRules _gameRules;
    private Vector3 _movePoint;
    private bool _isStartSpawn;
    private bool _isFirstStart = true;//Для одной подписки на эвенты
    private void CheckCountSpawn()
    {
        if (_countSpawning >= _numberForAddComplecity)
        {
            foreach (var item in _allMonsters)
            {
                item.AddComplexity();
            }
            _countSpawning = 0;
            e_addComplexity?.Invoke();
        }
    }
    private void Awake()
    {
        _gameMode = GetComponentInParent<GameMode>();
        _gameRules = GetComponentInParent<GameRules>();
        _timeSpawn = GetComponent<TimeSpawnMonster>();

        _gameRules.e_startAction += () =>
        {
            OnSpawning();//Запускаю спавн монстров
        };
        _gameRules.e_stopAction += () =>
        {//Выключаю спавн монстров
            _isStartSpawn = false;
            //    StopCoroutine(StartSpawning());


        };
        _gameRules.e_restartGame += () =>
        {//Сброс всех данных монстров
            ResetMonsters();
        };
        #region Boosters


        //Обработчик бустеров
        _gameRules.e_killAll += () =>
        {
            foreach (var item in _allMonsters)
            {
                if (item.gameObject.activeSelf)
                    item.TakeDamage(999);
            }
        };
        _gameRules.e_killFirst += () =>
        {
            BaseMonster pt = _allMonsters.Find(x => x.gameObject.activeSelf == true);
            if (pt)
                pt.TakeDamage(999);
        };

        #endregion
    }
    private async void Start()
    {
        _isMonsterActive[0] = true;
        _isMonsterActive[1] = true;
        _isMonsterActive[2] = true;
        _gameMode.PoolSpawnMonster.e_sendMonster += (BaseMonster pt) => _allMonsters.Add(pt);

        await System.Threading.Tasks.Task.Delay(System.TimeSpan.FromSeconds(1));
        InitEvents();
    }
    private void Update()
    {
        foreach (var item in _allMonsters)
        {
            if (item.gameObject.activeSelf == true)
                item.MoveToPoint();
        }


        if (_isStartSpawn)
        {
            if (_currentTimer <= _timeSpawn.CurrentGetDelay)
                _currentTimer += 1 * Time.deltaTime;
            else if (_currentTimer >= _timeSpawn.CurrentGetDelay)
            {
                _currentTimer = 0;
                _countSpawning++;
                Spawning();
                CheckCountSpawn();
                _timeSpawn.NewGetDelay();
            }
        }//?Потому что, как и со спавнером бустера я не могу оставить Yield return new
    }
    private void InitEvents()
    {
        foreach (var item in _allMonsters)
        {
            item.e_sendDecrementSpawn += (int x) => e_sendDecrementSpawn?.Invoke(x);
        }
    }
    private void OnSpawning()
    {
        _timeSpawn.NewGetDelay();
        _isStartSpawn = true;
    }


    private void Spawning()
    {
        //Get Position;
        int indexPosition = Random.Range(0, _gameMode.WayPoints.AllWayPoint.Length);
        Vector3 position = _gameMode.WayPoints.GetRandomPoint().position;
        //Get Monster       
        GetMonster(indexPosition, position);
        e_playClipSpawnMonster?.Invoke();
    }

    private void GetMonster(int indexPosition, Vector3 position)
    {
        int index = Random.Range(0, 100);
        CheckContinue(_countTypeMonster[0], _poolSpawnMonster.CountEasy, out _isMonsterActive[0]);
        CheckContinue(_countTypeMonster[1], _poolSpawnMonster.CountMiddle, out _isMonsterActive[1]);
        CheckContinue(_countTypeMonster[2], _poolSpawnMonster.CountHard, out _isMonsterActive[2]);
        if (index > 0 && index < _percentEasy && _isMonsterActive[0])
        {
            GetFirtsMonster<EasyMonster>(indexPosition, position);
        }
        else if (index >= _percentEasy && index < _percentMiddle && _isMonsterActive[1])
        {
            GetFirtsMonster<MiddleMonster>(indexPosition, position);
        }
        else if (index >= _percentMiddle && _isMonsterActive[2])
        {
            GetFirtsMonster<HardMonster>(indexPosition, position);
        }
        else
        {
            foreach (var item in _allMonsters)
            {
                if (!item.gameObject.activeSelf)
                {
                    item.transform.position = position;
                    item.gameObject.SetActive(true);
                    e_sendIncrementSpawn?.Invoke();
                    return;
                }
            }
        }

    }
    private void CheckContinue(int countCur, int maxCount, out bool pt)
    {
        if (countCur == maxCount)
            pt = false;
        else
            pt = true;
    }
    private void GetFirtsMonster<T>(int indexPosition, Vector3 position) where T : BaseMonster
    {
        BaseMonster i = _allMonsters.Find(x => x.gameObject.activeSelf == false && x as T);
        if (i && !i.gameObject.activeSelf)//fix зачем лишняя проверка?
        {
            i.gameObject.SetActive(true);
            i.transform.position = position;
            e_sendIncrementSpawn?.Invoke();
            if (i as EasyMonster)
                _countTypeMonster[0]++;
            else if (i as MiddleMonster)
                _countTypeMonster[1]++;
            else if (i as HardMonster)
                _countTypeMonster[2]++;
        }
    }
    private void ResetMonsters()
    {
        foreach (var item in _allMonsters)
        {
            item.ResetDataMonster();
            item.gameObject.SetActive(false);
        }
        _isStartSpawn = false;
        _countTypeMonster[0] = 0;
        _countTypeMonster[1] = 0;
        _countTypeMonster[2] = 0;
    }



}
