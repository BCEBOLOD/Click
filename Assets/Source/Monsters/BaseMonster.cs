using UnityEngine;

public abstract class BaseMonster : MonoBehaviour, ITakeDamage
{
    public UnityEngine.Events.UnityAction<int> e_sendCurrentHP;//отправка в Ui текущего хп
    public UnityEngine.Events.UnityAction<Vector3> e_sendPosition;//Для включение эффекта смерти/спавна на точке
    public UnityEngine.Events.UnityAction e_sendPlayDieSound;// Проигрывание звука смерти 
    public UnityEngine.Events.UnityAction<int> e_sendDecrementSpawn;
    [SerializeField] private int _startMaxHp;
    protected int _currentHP, _maxHP;
    [SerializeField] private float _startMoveSpeed;
    protected float _currentMoveSpeed;
    protected int _rewardPoint;
    [SerializeField] private int _startRewardPoint = 3;
    private GameMode _gameMode;
    [SerializeField] private Vector3 _newPointMove = Vector3.zero;
    [SerializeField] private Vector3 _oldPointMove = Vector3.one;
    private bool _isStartGame = false;
    private void Start()
    {
        _gameMode = GetComponentInParent<GameMode>();
        _rewardPoint = _startRewardPoint;
        ResetDataMonster();
        _isStartGame = true;
        Vector3 direction = Vector3.RotateTowards(transform.forward, (_newPointMove - transform.position), 360, 0);
        transform.rotation = Quaternion.LookRotation(direction);
    }
    protected virtual void DeathMonster()
    {
        e_sendDecrementSpawn?.Invoke(_rewardPoint);
        print("Смерть");

        gameObject.SetActive(false);
    }
    public void TakeDamage(int damage)
    {
        _currentHP -= damage;
        if (_currentHP <= 0)
        {
            DeathMonster();
            _currentHP = 0;
            e_sendPosition?.Invoke(transform.position);
            e_sendPlayDieSound?.Invoke();
        }
        e_sendCurrentHP?.Invoke(_currentHP);
    }

    public abstract void AddComplexity();
    /*
    Анимации
    */
    private void GetWayPoint()
    {
        _newPointMove = _gameMode.WayPoints.GetRandomPoint().position;
        if (_newPointMove == _oldPointMove)
        {
            while (_newPointMove == _oldPointMove)
            {
                _newPointMove = _gameMode.WayPoints.GetRandomPoint().position;
            }
        }
        Vector3 direction = Vector3.RotateTowards(transform.forward, (_newPointMove - transform.position), 360, 0);
        transform.rotation = Quaternion.LookRotation(direction);


    }
    public void MoveToPoint()
    {
        transform.position = Vector3.MoveTowards(transform.position, _newPointMove, _currentMoveSpeed);
        if (Vector3.Distance(_newPointMove, transform.position) <= 0.1f)
        {
            _oldPointMove = _newPointMove;
            GetWayPoint();
        }
        transform.position += transform.position * _currentMoveSpeed * Time.deltaTime;

    }
    public void ResetDataMonster()
    {
        _maxHP = _startMaxHp;
        _currentHP = _startMaxHp;
        _currentMoveSpeed = _startMoveSpeed;
        _rewardPoint = _startRewardPoint;
    }
    private void OnEnable()
    {
        _currentHP = _maxHP;

        if (_isStartGame)
            GetWayPoint();

    }
    private void OnDisable()
    {
        _newPointMove = Vector3.zero;
        _oldPointMove = Vector3.one;
    }
}

