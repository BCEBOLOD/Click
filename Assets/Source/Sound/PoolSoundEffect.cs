using System.Linq;
using UnityEngine;

public class PoolSoundEffect : MonoBehaviour
{

    private AudioSource _source;
    [SerializeField] private AudioClip _dieClipMonster;
    [SerializeField] private AudioClip _spawnMonsterClip;
    [SerializeField] private AudioClip _spawnBoosterClip;
    [SerializeField] private float _volumeStart;

    private GameRules _gameRules;
    private void Awake()
    {
        _gameRules = GetComponentInParent<GameRules>();
        _source = GetComponent<AudioSource>();
    }

    private void Start()
    {
        _source.volume = _volumeStart;
        _gameRules.e_playClipSpawnBooster += () => StartPlaySound(_spawnBoosterClip);
        _gameRules.SpawningMonster.e_playClipSpawnMonster += () => StartPlaySound(_spawnMonsterClip);
        _gameRules.GameMode.e_playClipDieMonster += () => StartPlaySound(_dieClipMonster);
    }

    private void StartPlaySound(AudioClip clip)
    {
        _source.PlayOneShot(clip);
    }
}
