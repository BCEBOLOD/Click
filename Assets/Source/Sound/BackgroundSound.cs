using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSound : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip[] BackgroundMusic;
    private int _penultimateIndexMusic;//Предпоследняя песня
    private int _lastIndexMusic;//Последняя песня
    private bool _isFirstStartMusic;//Первый старт для проигрывания музыки?
    private bool _isSavePenultimate;// был ли сохранен предсполедний?
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }
    private void Start()
    {
        _isFirstStartMusic = true;
        audioSource.volume = 0.3f;
        StartPlayMusic();
    }


    System.Collections.IEnumerator playAudio()
    {
        yield return new WaitForSeconds(audioSource.clip.length);
        FinishPlaySound();
    }

    private void FinishPlaySound()
    {
        StartPlayMusic();
    }
    private void StartPlayMusic()
    {
        audioSource.clip = RandomMusicPlay();
        audioSource.Play();
        StartCoroutine(playAudio());
    }

    public AudioClip RandomMusicPlay()
    {
        int getIndexClip;
        AudioClip audio = null;
        if (_isFirstStartMusic)
        {
            getIndexClip = Random.Range(0, BackgroundMusic.Length);
            audio = BackgroundMusic[getIndexClip];
            _lastIndexMusic = getIndexClip;
            _isFirstStartMusic = false;

        }
        else
        {
            if (!_isSavePenultimate)
            {
                getIndexClip = Random.Range(0, BackgroundMusic.Length);
                if (getIndexClip == _lastIndexMusic)
                {
                    while (getIndexClip == _lastIndexMusic)
                    {
                        getIndexClip = Random.Range(0, BackgroundMusic.Length);
                    }

                    audio = BackgroundMusic[getIndexClip];
                    _penultimateIndexMusic = _lastIndexMusic;
                    _lastIndexMusic = getIndexClip;
                    _isSavePenultimate = true;

                }
                else
                {
                    audio = BackgroundMusic[getIndexClip];
                    _penultimateIndexMusic = _lastIndexMusic;
                    _lastIndexMusic = getIndexClip;
                    _isSavePenultimate = true;
                }
            }
            else
            {
                getIndexClip = Random.Range(0, BackgroundMusic.Length);
                if (getIndexClip == _lastIndexMusic || getIndexClip == _penultimateIndexMusic)
                {
                    while (getIndexClip == _lastIndexMusic || getIndexClip == _penultimateIndexMusic)
                    {
                        getIndexClip = Random.Range(0, BackgroundMusic.Length);
                    }
                    audio = BackgroundMusic[getIndexClip];
                    _penultimateIndexMusic = _lastIndexMusic;
                    _lastIndexMusic = getIndexClip;

                }
                else
                {
                    audio = BackgroundMusic[getIndexClip];
                    _penultimateIndexMusic = _lastIndexMusic;
                    _lastIndexMusic = getIndexClip;
                }
            }

        }
        if (audio == null)
            throw new System.Exception("Music not founde - script Manager Static.cs");
        return audio;
    }

}
