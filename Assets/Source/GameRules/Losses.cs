using UnityEngine;

public class Losses : MonoBehaviour
{
    [SerializeField] private GameObject _LossesPanel;
    [SerializeField] private TMPro.TextMeshProUGUI _points;
    private GameRules _gameRules;
    private GameMode _gamemode;

    private void Awake()
    {
        _gameRules = GetComponentInParent<GameRules>();
        _gamemode = GetComponentInParent<GameMode>();
    }
    private void Start()
    {
        _gameRules.e_stopAction += () => { _LossesPanel.SetActive(true); _points.text = _gamemode.BalancePoint.ToString() + " очков";};
    }
}
