using UnityEngine;
public class GameRules : MonoBehaviour
{
    public UnityEngine.Events.UnityAction e_startAction;//Включение кнопок
    public UnityEngine.Events.UnityAction e_stopAction;//Выключение кнопок
    public UnityEngine.Events.UnityAction e_restartGame;//Сброс всех данных в ноль
                                                        //
    public UnityEngine.Events.UnityAction e_killAll;
    public UnityEngine.Events.UnityAction e_killFirst;
    public UnityEngine.Events.UnityAction e_addDelay;
    public UnityEngine.Events.UnityAction e_playClipSpawnBooster;
    //
    public FirstTouch FirstTouch => _firstTouch;
    public SpawningMonster SpawningMonster => _spawningMonster;
    public int MaxCountMonsterForLoss => _maxCountMonsterForLoss;

    [SerializeField] private FirstTouch _firstTouch;

    [SerializeField] private int _maxCountMonsterForLoss;
    private ControllerCamera _controllerCamera;
    private SpawningMonster _spawningMonster;
    private CountdownStart _countdownStart;
    private GameMode _gameMode;
    public GameMode GameMode => _gameMode;
    public ControllerCamera ControllerCamera => _controllerCamera;
    private void Awake()
    {
        _gameMode = GetComponent<GameMode>();
        _countdownStart = GetComponentInChildren<CountdownStart>();
        _controllerCamera = _firstTouch.GetComponent<ControllerCamera>();
        _spawningMonster = GetComponentInChildren<SpawningMonster>();
    }
    private void Start()
    {
        _countdownStart.e_sendStartGameplay += () => e_startAction?.Invoke();
        _controllerCamera.RaycastController.e_sendBooster += BoosterHandler;
    }

    private void BoosterHandler(BaseBooster booster)
    {
        if (booster as Booster_KillAll)
            e_killAll?.Invoke();
        else if (booster as Booster_AddDelay)
            e_addDelay?.Invoke();
        else if (booster as Booster_KillFirst)
            e_killFirst?.Invoke();

        e_playClipSpawnBooster?.Invoke();
    }
}
