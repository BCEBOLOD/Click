using UnityEngine;

public class TimeSpawnMonster : MonoBehaviour
{
    public float CurrentGetDelay => _currentDelaySpawn;

    [SerializeField] private float _currentDelaySpawn;
    [SerializeField] private float _startDelaySpawn;
    [Space(30)]
    [SerializeField] private float _startMinDelay;
    [SerializeField] private float _startMaxDelay;
    [SerializeField] private float _minDelay;
    [SerializeField] private float _maxDelay;
    [Space(30)]
    [SerializeField] private float _minSubstract;
    [SerializeField] private float _minMaxSubstract;
    [Space(30)]
    [SerializeField] private float _maxMinSubstract;
    [SerializeField] private float _maxMaxSubstract;
    private SpawningMonster _spawning;
    private void Awake()
    {
        _spawning = GetComponent<SpawningMonster>();
    }
    private void Start()
    {
        _minDelay = _startMinDelay;
        _maxDelay = _startMaxDelay;
        _currentDelaySpawn = _startDelaySpawn;
        _spawning.GameRules.e_addDelay += BoosterAddDelay;
        _spawning.e_addComplexity += AddComplexity;
        _spawning.GameRules.e_restartGame += () =>
        {
            _currentDelaySpawn = _startDelaySpawn;
            _minDelay = _startMinDelay;
            _maxDelay = _startMaxDelay;
        };
       // NewGetDelay();
    }
    public float NewGetDelay() => _currentDelaySpawn = Random.Range(_minDelay, _maxDelay);

    private async void BoosterAddDelay()
    {
        float current = _currentDelaySpawn;
        _currentDelaySpawn += 3;
        await System.Threading.Tasks.Task.Delay(System.TimeSpan.FromSeconds(_currentDelaySpawn + _currentDelaySpawn));
        _currentDelaySpawn = current;
    }
    private void AddComplexity()
    {
        _minDelay -= Random.Range(_minSubstract, _minMaxSubstract);
        _maxDelay -= Random.Range(_maxMinSubstract, _maxMaxSubstract);
    }

}
