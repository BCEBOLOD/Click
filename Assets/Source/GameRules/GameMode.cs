using UnityEngine;

public partial class GameMode : MonoBehaviour
{
    public UnityEngine.Events.UnityAction e_playClipDieMonster;
    //    
    public WayPoints WayPoints => _wayPoints;
    public PoolSpawnMonster PoolSpawnMonster => _poolSpawnMoster;
    public int BalancePoint => _balancePoint;
    //
    //Init monster
    private WayPoints _wayPoints;
    private GameRules _gameRules;

    private PoolSpawnMonster _poolSpawnMoster;
    private SpawningMonster _spawningMonster;
    private int _balancePoint;
    private int _countSpawningMonster;

    //End init Monster   

    [SerializeField] private CountdownStart _countdownstart;    //

    [SerializeField] private GameObject _mainMenu;
    [SerializeField] private GameObject _buttonsPanel;
    [SerializeField] private UnityEngine.UI.Button[] _otherButtons;
    [SerializeField] private GameObject _recordsPanel;
    [SerializeField] private GameObject _titlesPanel;
    [SerializeField] private GameObject _textPanel;
    [SerializeField] private TMPro.TextMeshProUGUI _numberLiveMonster;
    [SerializeField] private TMPro.TextMeshProUGUI _balancePointText;
    [SerializeField] private TMPro.TextMeshProUGUI _balanceHeadMenu;
    //


    private void Awake()
    {
        _wayPoints = GetComponentInChildren<WayPoints>();
        _poolSpawnMoster = GetComponentInChildren<PoolSpawnMonster>();
        _spawningMonster = GetComponentInChildren<SpawningMonster>();
        _countdownstart = GetComponentInChildren<CountdownStart>();
        _gameRules = GetComponent<GameRules>();
        //
        _spawningMonster.e_sendIncrementSpawn += CountingSpawnMonster;
        _spawningMonster.e_sendDecrementSpawn += DieMonster;
    }

    private void Start()
    {
        _otherButtons[0].onClick.AddListener(() => _gameRules.e_restartGame?.Invoke());// Restart game 
        _otherButtons[1].onClick.AddListener(() => { _recordsPanel.SetActive(true); _buttonsPanel.SetActive(false); });//records
        _otherButtons[2].onClick.AddListener(() => { _titlesPanel.SetActive(true); _buttonsPanel.SetActive(false); });//titles
        _otherButtons[3].onClick.AddListener(() => Application.Quit());//Exit
        _otherButtons[4].onClick.AddListener(() => OpenMainMenuButt());//Open Main menu
        _otherButtons[5].onClick.AddListener(() => CloseMainMenuButt());//Close main menu
        //
        _gameRules.e_stopAction += StopAction;
        _gameRules.e_restartGame += RestartAction;
        _gameRules.e_startAction += StartAction;

    }

    private void DieMonster(int point)
    {
        _countSpawningMonster--;
        _balancePoint += point;
        SetupTextData();
        e_playClipDieMonster?.Invoke();
    }

    private void CountingSpawnMonster()
    {
        _countSpawningMonster++;
        SetupTextData();
        if (_countSpawningMonster >= _gameRules.MaxCountMonsterForLoss)
        {
            _gameRules.e_stopAction?.Invoke();
        }
    }
    private void StartNewGame()
    {
        _balancePoint = 0;
        _countSpawningMonster = 0;
        _buttonsPanel.SetActive(false);
        _mainMenu.SetActive(false);
        _textPanel.SetActive(false);

        SetupTextData();
    }

    private void SetupTextData()
    {
        _numberLiveMonster.text = _countSpawningMonster + "/" + _gameRules.MaxCountMonsterForLoss;
        _balancePointText.text = _balancePoint.ToString();
    }

}
public partial class GameMode
{
    private void StopAction()
    {
        if (_otherButtons[5].gameObject.activeSelf)
            _otherButtons[5].gameObject.SetActive(false);

        _otherButtons[4].gameObject.SetActive(false);
        _buttonsPanel.gameObject.SetActive(true);
        _mainMenu.SetActive(true);
        _textPanel.SetActive(false);
    }
    private void StartAction()
    {
        SetupTextData();
        _textPanel.SetActive(true);
        _otherButtons[4].gameObject.SetActive(true);
    }
    private void RestartAction()
    {
        StartNewGame();
    }
    private void OpenMainMenuButt()//Кнопка открытие главного менюы   
    {
        _gameRules.ControllerCamera.e_stopActionGameplay?.Invoke();
        _otherButtons[4].gameObject.SetActive(false);
        _balanceHeadMenu.text = _balancePoint.ToString() + " очков";
        _buttonsPanel.SetActive(true);
        _mainMenu.SetActive(true);
        _otherButtons[5].gameObject.SetActive(true);
        _textPanel.SetActive(false);
    }
    private void CloseMainMenuButt()
    {
        _gameRules.ControllerCamera.e_startActionGameplay?.Invoke();
        _mainMenu.SetActive(false);
        _otherButtons[5].gameObject.SetActive(false);
        _otherButtons[4].gameObject.SetActive(true);
        _textPanel.SetActive(true);

    }
}