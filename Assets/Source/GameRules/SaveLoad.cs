using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using System.IO;
public class SaveLoad : MonoBehaviour
{
    //  [SerializeField] private TextAsset textJson;
    [SerializeField] private RecordList _reclis;
    [SerializeField] private GameRules _gameRules;
    [SerializeField] private System.DateTime _datatime;
    [SerializeField] private RecordList recordList = new RecordList();
    [SerializeField] public List<RecordsData> _listRecord;
    [SerializeField] private List<TMPro.TextMeshProUGUI> _positionRecord;


    [SerializeField] private string _path;
    private int _balance;


    private void Awake()
    {

        _gameRules = GetComponent<GameRules>();
        _path = Application.persistentDataPath + "/RecordData.json";
        print(_path);
    }
    private void Start()
    {

        _gameRules.e_stopAction += (() => { TrySave(); });
        Load();

    }


    [ContextMenu("Load")]
    public async void Load()
    {

        if (File.Exists(_path))
        {
            string pt = File.ReadAllText(_path); //JsonUtility.FromJson<RecordList>(Application.streamingAssetsPath + "/RecordData.json");
            recordList = JsonUtility.FromJson<RecordList>(pt);
            foreach (var item in recordList.RecordsData)
            {
                _listRecord.Add(item);
            }


            SortingRecord();
            InitRecordData();
        }
        else
        {

            File.Create(_path);
            _listRecord = new List<RecordsData>() { new RecordsData { Score = 0, Data = "" }, new RecordsData { Score = 0, Data = "0" }, new RecordsData { Score = 0, Data = "0" } };
            await System.Threading.Tasks.Task.Delay(TimeSpan.FromSeconds(0.3));

            foreach (var item in _listRecord)
            {
                item.Score = 0;
                item.Data = "0,0,0";
            }

            recordList.RecordsData = _listRecord.ToArray();
            string pt = JsonUtility.ToJson(recordList, true);

            File.WriteAllText(_path, pt);
        }



    }


    private void SetupRecord(int index)
    {
        _datatime = DateTime.Now;
        _balance = _gameRules.GameMode.BalancePoint;

        _listRecord[index].Score = _balance;
        _listRecord[index].Data = _datatime.ToString();
    }
    [ContextMenu("Save")]
    public void TrySave()
    {

        _datatime = DateTime.Now;
        _balance = _gameRules.GameMode.BalancePoint;
        for (int i = 2; i != _listRecord.Count; i--)
        {
            int ind = 0;
            if (i >= 1)
                ind = i - 1;
            else
                ind = i;

            if (i == 0)
            {
                _listRecord.Insert(i, new RecordsData { Score = _balance, Data = _datatime.ToString() });
                _listRecord.RemoveAt(3);
                break;
            }
            if (_balance == _listRecord[i].Score)
            {

                _listRecord[i].Score = _balance;
                _listRecord[i].Data = _datatime.ToString();
                break;
            }
            else if (_balance > _listRecord[i].Score && _balance < _listRecord[ind].Score)
            {
                _listRecord.Insert(i, new RecordsData { Score = _balance, Data = _datatime.ToString() });
                _listRecord.RemoveAt(3);
                break;
            }
            else if (_balance > _listRecord[i].Score && _balance > _listRecord[ind].Score)
            {
                continue;
            }
            else
                break;

        }
        SortingRecord();
        recordList.RecordsData = _listRecord.ToArray();
        string pt = JsonUtility.ToJson(recordList, true);

        File.WriteAllText(_path, pt);
        InitRecordData();
    }
    public void SaveIntoJson()
    {
        string potion = JsonUtility.ToJson(recordList, true);
        System.IO.File.WriteAllText(_path, potion);
    }


    private void SortingRecord()
    {
        _listRecord = _listRecord.OrderByDescending(x => x.Score).ToList();

    }
    private void InitRecordData()
    {
        for (int i = 0; i < _positionRecord.Count; i++)
        {
            _positionRecord[i].text = "Очков " + _listRecord[i].Score + "    Дата " + _listRecord[i].Data;
        }
    }
    [System.Serializable]
    public class RecordList
    {
        public RecordsData[] RecordsData;
    }

    [System.Serializable]
    public class RecordsData
    {
        public string Data;
        public int Score;
    }


}
