using UnityEngine;
using UnityEngine.EventSystems;
public class FirstTouch : MonoBehaviour
{
    public UnityEngine.Events.UnityAction e_startWave;
    [SerializeField] private TMPro.TextMeshProUGUI _textForStart;
    private Touch _touch;
    private void Start()
    {
        _textForStart.text = "Проведите пальцем по экрану, чтобы игра началась";
    }

    private void FixedUpdate()
    {
        if (Input.touchCount == 1)
        {
            {
                _touch = Input.GetTouch(0);
                if (_touch.phase == TouchPhase.Moved)
                {
                    e_startWave?.Invoke();
                    enabled = false;
                }
            }
        }
    }

}
