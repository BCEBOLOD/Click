using UnityEngine;
public class RaycastController : MonoBehaviour
{
    public UnityEngine.Events.UnityAction<BaseBooster> e_sendBooster;
    [SerializeField] private Camera _camera;
    private bool _canSpawnRay = false;
    private ControllerCamera _controllerCamera;

    private void Awake()
    {
        _camera = GetComponent<Camera>();
        _controllerCamera = GetComponent<ControllerCamera>();
    }

    private void Start()
    {
        _controllerCamera.e_startActionGameplay += () => { enabled = true; _canSpawnRay = true; };
        _controllerCamera.e_stopActionGameplay += () => { enabled = false; _canSpawnRay = false; };
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && _canSpawnRay)
        {
            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit, 1000))
            {
                if (hit.collider.TryGetComponent(out BaseMonster monster))
                {
                    monster.TakeDamage(1);
                }
                else if (hit.collider.TryGetComponent(out BaseBooster booster))
                {
                    e_sendBooster?.Invoke(booster);
                    booster.Action(); 
                }
            }
        }
    }

}
