using UnityEngine;

public class ControllerCamera : MonoBehaviour
{
    public UnityEngine.Events.UnityAction e_startActionGameplay;
    public UnityEngine.Events.UnityAction e_stopActionGameplay;
    [SerializeField] private GameRules _gameRules;
    private CameraMove _cameraMove;
    private CameraTouch _cameraTouch;
    private RaycastController _raycastCamera;
    private FirstTouch _firstTouch;
    public RaycastController  RaycastController => _raycastCamera;

    private void Awake()
    {
        _firstTouch = GetComponent<FirstTouch>();
        _cameraMove = GetComponent<CameraMove>();
        _cameraTouch = GetComponent<CameraTouch>();
        _raycastCamera = GetComponent<RaycastController>();
    }

    private void Start()
    {
        _gameRules.e_startAction += () => { e_startActionGameplay?.Invoke(); };
        _gameRules.e_stopAction += () => e_stopActionGameplay?.Invoke();
        OnDisableAll();
    }
    private void OnDisableAll()
    {
        _raycastCamera.enabled = false;
        _cameraMove.enabled = false;
        _cameraTouch.enabled = false;
    }

}


