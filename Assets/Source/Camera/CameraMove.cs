using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    private ControllerCamera _controller;
    [SerializeField] private float _speed;
    [SerializeField] private float _leftClamp, _rightClamp;
    private bool _canMove = false;
    private void Awake() => _controller = GetComponent<ControllerCamera>();
    private void Start()
    {
        _controller.e_startActionGameplay += () => { _canMove = true; enabled = true; };
        _controller.e_stopActionGameplay += () => { _canMove = false; enabled = false; };
    }

    public void MoveCamera(Touch touch)
    {
        if (_canMove)
        {
            Vector3 movePos = new Vector3
                  (transform.position.x, transform.position.y,
                   transform.position.z + touch.deltaPosition.x * _speed *  Time.deltaTime);
            Vector3 distance = movePos - transform.position;
            transform.position += distance;

            transform.position = new Vector3(
                transform.position.x, transform.position.y,
                Mathf.Clamp(transform.position.z, _leftClamp, _rightClamp)
            );
        }
    }
}
