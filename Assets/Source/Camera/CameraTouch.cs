using UnityEngine;
public class CameraTouch : MonoBehaviour
{
    public UnityEngine.Events.UnityAction e_sendOnSpawnMonster;
    [SerializeField] private CameraMove _cameraMove;
    [SerializeField] private ControllerCamera _controllerCamera;
    private Touch _touch;
    private bool _isStartSpawn;

    private void Awake()
    {
        _controllerCamera = GetComponent<ControllerCamera>();
        _cameraMove = GetComponent<CameraMove>();
    }

    private void Start()
    {
        _controllerCamera.e_startActionGameplay += () => { enabled = true; };
        _controllerCamera.e_stopActionGameplay += () => { enabled = false; };
    }
    private void Update()
    {
        if (Input.touchCount == 1)
        {
            _touch = Input.GetTouch(0);

            if (_touch.phase == TouchPhase.Moved)
            {
                _cameraMove.MoveCamera(_touch);
            }
        }

    }

}
