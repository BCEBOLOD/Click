using UnityEngine;
using System.Threading;
public class SpawnerBooster : MonoBehaviour
{
    [SerializeField] private System.Collections.Generic.List<BaseBooster> _poolBoosters;
    [SerializeField] private int _countSpawn;
    [SerializeField] private GameObject[] _allBoosters;
    [SerializeField] private float _minDelay, _maxDelay;
    [SerializeField] private float _currentTimer;
    private bool _startSpawn;
    private int _index;
    private GameMode _gameMode;
    private GameRules _gameRules;
    private void Awake()
    {
        _gameMode = GetComponentInParent<GameMode>();
        _gameRules = GetComponentInParent<GameRules>();
    }
    private void Start()
    {
        InitPool();
        _gameRules.e_startAction += () => { _startSpawn = true; };
        _gameRules.e_stopAction += () => { _startSpawn = false; };
        _gameRules.e_restartGame += RestartAction;

        _currentTimer = Random.Range(_minDelay, _maxDelay);
    }
    private void RestartAction()
    {
        _startSpawn = false;
        _currentTimer = Random.Range(_minDelay, _maxDelay);
        foreach (var item in _poolBoosters)
        {
            if (item.gameObject.activeSelf)
                item.gameObject.SetActive(false);
        }
    }
    private void Update()
    {
        if (_startSpawn)
        {
            if (_currentTimer > 0)
            {
                _currentTimer -= 1 * Time.deltaTime;
                return;
            }
            else if (_currentTimer <= 0)
            {
                SpawnBooster();
                _currentTimer = Random.Range(_minDelay, _maxDelay);
            }

        }

    }
    private void FixedUpdate()
    {
        foreach (var item in _poolBoosters)
        {
            if (item.gameObject.activeSelf)
                item.Rotation();
        }
    }
    private void InitPool()
    {
        int indexInside = 0;
        for (int i = 0; i < _countSpawn; i++)
        {
            if (indexInside >= 3)
            {
                _index++;
                indexInside = 0;
            }
            GameObject pt = Instantiate(_allBoosters[_index], transform.position, Quaternion.identity);
            pt.transform.SetParent(transform);
            _poolBoosters.Add(pt.GetComponent<BaseBooster>());
            pt.SetActive(false);
            indexInside++;
        }
    }   
    private void SpawnBooster()
    {

        int type = Random.Range(1, 4);//AddDelay,KillAll,KillOne;
        print(type);
        if (type == 1)
            GetBooster<Booster_AddDelay>();
        else if (type == 2)
            GetBooster<Booster_KillAll>();
        else if (type == 3)
            GetBooster<Booster_KillFirst>();
    }
    private void GetBooster<T>() where T : BaseBooster
    {
        BaseBooster boost = _poolBoosters.Find(x => x.gameObject.activeSelf == false && x as T);
        if (boost)
        {
            boost.gameObject.SetActive(true);
            boost.transform.position = _gameMode.WayPoints.GetRandomPoint().position;
        }
    }
}

