using UnityEngine;

public abstract class BaseBooster : MonoBehaviour
{
    [SerializeField] private float _speedRotate;
    private float y;
    public void Rotation()
    {
        y += _speedRotate * Time.deltaTime;
        transform.rotation = Quaternion.Euler(0, y, 0);
    }
    public virtual void Action()
    {
        gameObject.SetActive(false);
    }
    private void OnSpawnAction()
    {
        //Брать из пула эффектов первый свободный установить в позицию спавна
        //Брать из пула звуков первый свободный звук и проиграть на нем звук и в конце выключить.
    }


}
